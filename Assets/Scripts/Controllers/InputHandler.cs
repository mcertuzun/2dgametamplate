﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [Header("Inputs")]
    [SerializeField] private float horizontal, vertical, moveInput;
    [SerializeField] private bool leftClick;
    [SerializeField] private bool spaceKey;

    StateManager stateManager;

    private void Start()
    {
        //Fetching state manager
        stateManager = GetComponent<StateManager>();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    //Player inputs
    void GetInput()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
        leftClick = Input.GetMouseButtonDown(0);
        spaceKey = Input.GetKeyDown(KeyCode.Space);
        UpdateStates();
    }

    //Sending inputs to state manager
    void UpdateStates()
    {
        stateManager.horizontal = horizontal;
        stateManager.vertical = vertical;
        stateManager.leftClick = leftClick;
        stateManager.spaceKey = spaceKey;
        stateManager.moveInput = new Vector2(horizontal, vertical);
    }


}
