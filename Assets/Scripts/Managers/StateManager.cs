﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{

    [Header("Inputs")]
    [HideInInspector] public float horizontal, vertical;
    [HideInInspector] public bool leftClick;
    [HideInInspector] public bool spaceKey;
    [HideInInspector] public Vector2 moveInput;
    public Transform feetPos;
    public float checkRadius; //On ground check radius
    public LayerMask whichLayer;

    [Header("Stats")]
    [SerializeField] private float playerSpeed;
    [SerializeField] private Vector2 moveAmount;
    [SerializeField] private float jumpForce = 8;
    [SerializeField] private int jumpCount;

    [Header("States")]
    [SerializeField] private bool isGrounded;
    [SerializeField] private bool isJumping;

    [Header("Components")]
    private Rigidbody2D rb;
    private Animator anim;
    private float screenWidth;
    // Start is called before the first frame update
    private void Start()
    {
        screenWidth = Screen.width;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
       
        move();
        Jumping();
        animationHandler();
        
        
    }

    private void FixedUpdate()
    {
        //Player movement techniques

        /* First one takes changes in both x and y 
        rb.MovePosition(rb.position + moveAmount * Time.fixedDeltaTime);
        rb.velocity = new Vector2(horizontal * playerSpeed, rb.velocity.y);
        */
    }

    private void move()
    {

        /*  while (0 < Input.touchCount)
          {
              if (Input.GetTouch(0).position.x < Screen.width / 2 && Input.GetTouch(0).position.x < Screen.height / 2)
              {
                  rb.velocity = new Vector2(-1.0f * 5 * Time.deltaTime, rb.velocity.y);
              }
              if (Input.GetTouch(0).position.x > Screen.width /2 && Input.GetTouch(0).position.x < Screen.height /2)
              {
                  rb.velocity = new Vector2(1.0f * 5 * Time.deltaTime, rb.velocity.y);
              }

          }
          */
        rb.velocity = new Vector2(horizontal * playerSpeed, rb.velocity.y);

    }

    private void Jumping()
    {
        //It is for the first technique
        //moveAmount = moveInput.normalized * playerSpeed;

        //OverlapCircle is a circle which is founded in a given position. When it is triggering from a layer which you choosed, isGrounded is changing as true.
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whichLayer);


        //double jump implemented
        if (isGrounded && spaceKey || jumpCount < 1 && spaceKey)
        {
            jumpCount++;
            anim.SetTrigger("takeOf");
            rb.velocity = Vector2.up * jumpForce;    
        }
        if (isGrounded) 
        {
            jumpCount = 0;
        }
        else { anim.SetBool("isJumping", true); }
    
    }
    private void animationHandler()
    {
        if (horizontal == 0)
        {
            anim.SetBool("isRunning", false);
        }
        else { anim.SetBool("isRunning", true); }
    }

}
