﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelManagement;

namespace SampleGame
{
    public class GameManager : MonoBehaviour
    {
        // reference to player
        // private ThirdPersonCharacter _player;
        private GameObject player;
        // reference to goal effect
        private GoalEffect goalEffect;
        // reference to player
        private Objective objective;


        private bool isGameOver;
        public bool IsGameOver { get { return isGameOver; } }

        //Singleton pattern-1: You must create static instance and make it global with setters and getter.
        private static GameManager instance;
        public static GameManager Instance { get { return instance; } }

        // initialize references
        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
            }
            player = GameObject.FindGameObjectWithTag("Player");
            objective = Object.FindObjectOfType<Objective>();
            goalEffect = Object.FindObjectOfType<GoalEffect>();
        }
        private void OnDestroy()
        {
            if (instance == this)
            {
                instance = null;
            }
        }
    
        // check for the end game condition on each frame
        private void Update()
        {
            if (objective != null && objective.IsComplete)
            {
                EndLevel();
            }
        }

        // end the level
        public void EndLevel()
        {
               if (player != null)
               {
                // disable the player controls
                InputHandler inputHandler = player.GetComponent<InputHandler>();
                          
                   if (inputHandler != null)
                   {
                    inputHandler.enabled = false;
                   }

                   // remove any existing motion on the player
                   Rigidbody rbody = player.GetComponent<Rigidbody>();
                   if (rbody != null)
                   {
                       rbody.velocity = Vector2.zero;
                   }
                 
                // force the player to a stand still
                
               }

               // check if we have set IsGameOver to true, only run this logic once
               if (goalEffect != null && !isGameOver)
               {              
                isGameOver = true;
                goalEffect.PlayEffect();

                WinScreen.Open();         
            }

        }

        

    }
}