﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SampleGame;

namespace LevelManagement
{
    public class WinScreen : Menu<WinScreen>
    {
        public void OnNextLevelPressed()
        {
            base.OnPressBack();
            LevelManager.LoadNextLevel();
        }
        public void OnRestartPressed()
        {
            base.OnPressBack();
            LevelManager.RestartLevel();
        }
        public void OnMainMenuPressed()
        {
            base.OnPressBack();
            LevelManager.LoadLevel(LevelManager.mainMenuIndex);
        }
    }

}
