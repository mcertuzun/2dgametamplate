﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelManagement;
using System;


namespace utilities
{
    public class SplashScreen : MonoBehaviour
    {
        private ScreenFader screenFader;

        private void Awake()
        {
            screenFader = GetComponent<ScreenFader>();         
        }
        private void Start()
        {
            fadeInOpenning();
        }
        private void fadeInOpenning()
        {
            StartCoroutine(openGame());
        }

        private IEnumerator openGame()
        {
            screenFader.SetAlpha(0f);
            screenFader.FadeOn();
            //LevelManager.LoadNextLevel();
            yield return new WaitForSeconds(1.8f);
            LevelManager.LoadNextLevel();

        }   
    }
}