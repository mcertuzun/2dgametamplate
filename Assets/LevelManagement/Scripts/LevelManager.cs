﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LevelManagement
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] public static int mainMenuIndex = 1;

 
        public static void GetBackMainMenu()
        {
            MainMenu.Open();
            SceneManager.LoadScene(mainMenuIndex);
        }

        public static void LoadLevel(int levelIndex)
        {
            if(levelIndex == 0)
            {
                GetBackMainMenu();
            }
            if (levelIndex > 0 && levelIndex < SceneManager.sceneCountInBuildSettings)
            {
                if (levelIndex == mainMenuIndex)
                {
                    MainMenu.Open();
                }
                SceneManager.LoadScene(levelIndex);
            }
            else
            {
                Debug.LogWarning("Loading error: scene.");
            }
        } 

        public static void LoadNextLevel()
        {
            int nextSceneIndex = (SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings;
            Debug.Log(nextSceneIndex);
            LoadLevel(nextSceneIndex);

        }

        public static void RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

}
